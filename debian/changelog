goaccess (1:1.2-5) UNRELEASED; urgency=medium

  * d/copyright: Use https protocol in Format field
  * d/changelog: Remove trailing whitespaces

 -- Ondřej Nový <onovy@debian.org>  Mon, 01 Oct 2018 09:50:54 +0200

goaccess (1:1.2-4) unstable; urgency=medium

  [ Federico Ceratto ]
  * Remove duplicate dependency
  * Remove unneded dependencies

  [ Antonio Terceiro ]
  * Bump Standards-Version to 4.1.4
    * Set Priority: from extra to optional
  * Vcs-*: switch to salsa.debian.org
  * Bump debhelper compatibility level to 11
  * Add a basic autopkgtest smoke test
  * Recommend DFSG-free geoip databases
  * debian/resources.mk: adapt to fonts-font-awesome >= 5

 -- Antonio Terceiro <terceiro@debian.org>  Sat, 12 May 2018 20:37:19 -0300

goaccess (1:1.2-3) unstable; urgency=medium

  * debian/gen-built-using: list source packages instead of binaries
    (Closes: #864928)
  * Bump Standards-Version: to 3.9.8; no changes needed otherwise
  * Bump debhelper compatibility level to 10

 -- Antonio Terceiro <terceiro@debian.org>  Mon, 19 Jun 2017 09:55:29 -0300

goaccess (1:1.2-2) unstable; urgency=medium

  * Upload to unstable

 -- Antonio Terceiro <terceiro@debian.org>  Sun, 18 Jun 2017 13:38:26 -0300

goaccess (1:1.2-1) experimental; urgency=medium

  [ Martin Bagge / brother ]
  * Fix debian/watch (Closes: #848954)

  [ Antonio Terceiro ]
  * New upstream version 1.2 (Closes: #858292)
    - pre-minified Javascript and CSS files removed; we use the existing
      source packages in Debian
  * wrap-and-sort
  * debian/rules:
    - enable Tokyocabinet support (Closes: #857804)
    - use dh-autoreconf
    - really enable geoip support
  * debian/copyright: convert to machine-readable format
    - note that upstream is now licensed under MIT/Expat license.
    - use Files-Excluded to make uscan remove pre-minified JS/CSS from
      upstream tarball
  * debian/patches/0001-Makefile.am-don-t-install-resources.patch: don't
    install extra files to /usr/share/doc/
  * debian/patches/0002-Fix-db-path-without-trailing-slash.patch: ditto
  * Add myself to Uploaders:
  * Add Vcs-* fields pointing to collab-maint/goaccess
  * Add missing build dependency on libncurses5-dev
  * Add missing build dependency on libbz2-dev

 -- Antonio Terceiro <terceiro@debian.org>  Thu, 08 Jun 2017 20:46:39 -0300

goaccess (1:0.9.4-1) unstable; urgency=medium

  * New upstream release.
  * Bump standards-version.
  * Update debian/copyright.
     - Update dates.
  * Enabled geoip in build.

 -- Chris Taylor <ctaylor@debian.org>  Tue, 06 Oct 2015 16:06:24 +0000

goaccess (1:0.8.3-1) unstable; urgency=medium

  * New upstream release.
  * Update debian/copyright download location.
  * Update address in debian/watch.

 -- Chris Taylor <ctaylor@debian.org>  Sat, 30 Aug 2014 03:41:22 +0000

goaccess (1:0.8-1) unstable; urgency=low

  * New upstream release (Closes: #739719).
  * Bump standards-version.
  * Update debian/copyright.
     - Update dates.
  * Update debian/watch with new location.

 -- Chris Taylor <ctaylor@debian.org>  Fri, 30 May 2014 10:04:35 -0700

goaccess (1:0.6-1) unstable; urgency=low

  * New upstream release 0.6
  * Bump standards-version.
  * Update debian/copyright
    - Update dates.
    - Update author's email address.

 -- Chris Taylor <ctaylor@debian.org>  Sun, 28 Jul 2013 06:18:52 +0000

goaccess (1:0.5-1) unstable; urgency=low

  * New upstream release 0.5
  * Removed all patches as fixes are now upstream.

 -- Chris Taylor <ctaylor@debian.org>  Tue, 05 Jun 2012 05:18:30 +0000

goaccess (1:0.4.2-3) unstable; urgency=low

  * Bump compat to 9.
  * Bump versioned build-dep on dh.
  * Cleanup debian/rules.
  * Bump standards-version.

 -- Chris Taylor <ctaylor@debian.org>  Sun, 22 Apr 2012 19:17:59 +0000

goaccess (1:0.4.2-2) unstable; urgency=low

  * Remove patch 02-ld.
  * Add patch 00-ld-as-needed (Closes: #606207).
  * Bump standards-version.
  * Update debian/copyright.
  * Remove config.status and config.log via debian/rules.

 -- Chris Taylor <ctaylor@debian.org>  Wed, 24 Aug 2011 04:21:25 +0000

goaccess (1:0.4.2-1) unstable; urgency=low

  * New upstream release.
  * Fix problems in manpage in 01-manpage.

 -- Chris Taylor <ctaylor@debian.org>  Wed, 12 Jan 2011 10:07:19 +0000

goaccess (1:0.4-1) unstable; urgency=low

  * New upstream release.

 -- Chris Taylor <ctaylor@debian.org>  Tue, 07 Dec 2010 01:37:57 +0000

goaccess (1:0.3.3-1) unstable; urgency=low

  * New upstream release 0.3.3
  * Update debian/copyright.
  * Bump epoch to fix versioning.

 -- Chris Taylor <ctaylor@debian.org>  Sat, 23 Oct 2010 09:52:37 +0000

goaccess (0.3-1) unstable; urgency=low

  * New upstream release 0.3

 -- Chris Taylor <ctaylor@debian.org>  Mon, 30 Aug 2010 17:40:32 +0000

goaccess (0.12-1) unstable; urgency=low

  * New upstream release.

 -- Chris Taylor <ctaylor@debian.org>  Tue, 13 Jul 2010 17:51:09 +0000

goaccess (0.1-1) unstable; urgency=low

  * Initial release (Closes: #588499)

 -- Chris Taylor <ctaylor@debian.org>  Sat, 10 Jul 2010 17:42:59 -0700
